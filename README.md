### Elasticsearch max virtual memory areas vm.max_map_count issue 
Run in host machine:
```bash
$ sudo sysctl -w vm.max_map_count=262144
```